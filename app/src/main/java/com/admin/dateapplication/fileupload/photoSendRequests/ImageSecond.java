package com.admin.dateapplication.fileupload.photoSendRequests;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.admin.dateapplication.fileupload.R;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;

import com.android.volley.Request;

public class ImageSecond extends AppCompatActivity {

    //define global views variable
    public ImageView imageView, image, Image_path;
    public Button selectImage,

    uploadImage;
    public String SERVER = "http://192.168.1.2/co/storemanger/saveImage.php", timestamp;

    Bitmap bitmap;
    private static final String TAG = ImageSecond.class.getSimpleName();

    private static final int RESULT_SELECT_IMAGE = 1;
    private ProgressBar progress_bar;

    private RequestQueue requestQueue;
    private String image2;

    Button choose, upload;
    int PICK_IMAGE_REQUEST = 111;
    //    String URL ="http://192.168.1.101/JavaRESTfullWS/DemoService/upload";
//    public String URL = "http://motoport.in/motoport/Api/Image_upload";
    public String URL = "http://motoport.in/motoport/Api/upload_img";
    private ProgressDialog progressDialog;
    private TextView text_response;

    private String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_second);

        //instantiate view
        image = findViewById(R.id.imageView2);
        choose = findViewById(R.id.selectImage2);
        upload = findViewById(R.id.uploadImage2);
        text_response = findViewById(R.id.text_response);


        //when selectImage button is pressed
        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);


            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                imageRequest();


            }
        });


    }

    private void imageRequest() {
        final ProgressDialog progressDialog = new ProgressDialog(ImageSecond.this);
        progressDialog.setMessage("Uploading, please wait...");
        progressDialog.show();

//        //converting image to base64 string
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
////        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

       final String encodedImageData = getEncoded64ImageStringFromBitmap(bitmap);


        //sending image to server
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                progressDialog.dismiss();
                Toast.makeText(ImageSecond.this, "Mera response " + s, Toast.LENGTH_SHORT).show();
                text_response.setText(s);

//                if (s.equals("true")) {
//                    Toast.makeText(ImageSecond.this, "Uploaded Successful", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(ImageSecond.this, "Else wala error occurred!", Toast.LENGTH_LONG).show();
//                    progressDialog.dismiss();
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(ImageSecond.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Basic YWRtaW46MTIzNA==");

                return params;
            }

            //adding parameters to send
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<String, String>();

                parameters.put("Mobile", "8860229485");
                parameters.put("user_id", "U113668");
                parameters.put("img_front", encodedImageData);
                return parameters;
            }
        };

        RequestQueue rQueue = Volley.newRequestQueue(ImageSecond.this);
        rQueue.add(request);
    }


    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
       image.setImageBitmap(bitmap);
        return imgString;
    }

//    private String imageToString(Bitmap bitmap){
//
//        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
//        byte[] imgBytes=byteArrayOutputStream.toByteArray();
//        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri filePath = data.getData();

            try {
                //getting image from gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                //Setting image to ImageView
                image.setImageBitmap(bitmap);
                path = getPath(filePath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
